package com.example.inventoryserver.service;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.example.inventoryserver.utils.MemoryAppender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class ServiceWithLoggingTest {

    @InjectMocks
    private ServiceWithLogging service;

    private static final String LOGGER_NAME = ServiceWithLogging.class.getCanonicalName();
    private MemoryAppender memoryAppender;

    @Test
    void logSomething() {
        setUpLoggerAppender(Level.INFO);
        service.logSomething();
        memoryAppender.stop();

        assertThat(memoryAppender.countEventsForLogger(LOGGER_NAME)).isEqualTo(1);
        assertThat(memoryAppender.contains("I am logging something to test", Level.INFO)).isTrue();

    }

    public void setUpLoggerAppender(Level level) {
        Logger logger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        logger.setLevel(level);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

}