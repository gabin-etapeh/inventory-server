package com.example.inventoryserver.service;

import com.example.inventoryserver.model.Product;
import com.example.inventoryserver.repository.CategoryRepository;
import com.example.inventoryserver.model.Category;
import com.example.inventoryserver.repository.ProductRepository;
import com.zaxxer.hikari.util.FastList;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class CategoryService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    public Category getCategory(UUID id) {
        return categoryRepository.findById(id)
                .orElse(null);
    }

    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public void deleteCategory(UUID id) {
        List<Product> products = productRepository.getProductOnCategory(id);
        if (productRepository.getProductOnCategory(id).isEmpty()) {
            categoryRepository.deleteById(id);
        } else {
            products.forEach(product -> productRepository.deleteById(product.getId()));

            categoryRepository.deleteById(id);
        }

    }


    public Optional<Category> updateCategory(UUID id, Category nouvelleCategory) {
        Optional<Category> ancienneCategoryOptional = categoryRepository.findById(id);
        if (ancienneCategoryOptional.isPresent()) {

            Category ancienneCategory = ancienneCategoryOptional.get();
            ancienneCategory.setCategoryName(nouvelleCategory.getCategoryName());
            ancienneCategory.setDescription(nouvelleCategory.getDescription());
            return Optional.of(categoryRepository.save(ancienneCategory));
        } else {
            return Optional.empty();
        }
    }

}