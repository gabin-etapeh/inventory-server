package com.example.inventoryserver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ServiceWithLogging {

    public void logSomething() {
      log.info("I am logging something to test");
    }
}
