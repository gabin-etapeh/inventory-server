package com.example.inventoryserver.service;

import com.example.inventoryserver.model.Supplier;
import com.example.inventoryserver.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SupplierService {
    private final SupplierRepository supplierRepository;

    public Supplier addSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    public Supplier getSupplier(UUID id) {
        return supplierRepository.findById(id).orElse(null);
    }

    public List<Supplier> getSuppliers() {
        return supplierRepository.findAll();
    }

    public void deleteSupplier(UUID id) {
        supplierRepository.deleteById(id);
    }

    public Optional<Supplier> updateSupplier(UUID id, Supplier nouveauFournisseur) {
        Optional<Supplier> ancienFournisseurOptional = supplierRepository.findById(id);
        if (ancienFournisseurOptional.isPresent()) {

            Supplier ancienFournisseur = ancienFournisseurOptional.get();
            ancienFournisseur.setName(nouveauFournisseur.getName());
            ancienFournisseur.setDescription(nouveauFournisseur.getDescription());
            ancienFournisseur.setPhone(nouveauFournisseur.getPhone());
            ancienFournisseur.setCity(nouveauFournisseur.getCity());

            return Optional.of(supplierRepository.save(ancienFournisseur));
        } else {
            return Optional.empty();
        }
    }
}
