package com.example.inventoryserver.service;

import com.example.inventoryserver.model.Category;
import com.example.inventoryserver.model.Product;
import com.example.inventoryserver.model.StockMovement;
import com.example.inventoryserver.model.StockMovementLine;
import com.example.inventoryserver.model.dto.StockMovementDto;
import com.example.inventoryserver.model.dto.ProductDto;
import com.example.inventoryserver.model.enums.EnumMovementType;
import com.example.inventoryserver.repository.CategoryRepository;
import com.example.inventoryserver.repository.ProductRepository;
import com.example.inventoryserver.repository.StockMovementLineRepository;
import com.example.inventoryserver.repository.StockMovementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final StockMovementRepository stockMovementRepository;
    private final StockMovementLineRepository stockMovementLineRepository;

    public Product addProduct(ProductDto productDto) {
        // 1. Rechercher la catégorie dans laquelle créer le produit
        Optional<Category> categoryOptional
                = categoryRepository.findById(productDto.getCategoryId());

        // 2. Si on a trouvé la catégorie, créer le produit et lui ajouter la catégorie
        //    puis sauvegarder le produit
        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();

            Product product = new Product();
            product.setName(productDto.getName());
            product.setDescription(productDto.getDescription());
            product.setVariety(productDto.getVariety());
            product.setPrice(productDto.getPrice());
            product.setBarCode(productDto.getBarCode());
            product.setExpiryDate(productDto.getExpiryDate());
            product.setUnit(productDto.getUnit());
            product.setQuantity(productDto.getQuantity());
            product.setStockAlert(productDto.getStockAlert());
            product.setImageUrl(productDto.getImageUrl());

            product.setCategory(category);
            return productRepository.save(product);
        } else {
            // 3. Sinon renvoyer une exception
            throw new RuntimeException("The category you are trying to assign to the product does not" +
                    "exist");
        }
    }

    public Product getProduct(UUID id) {
        return productRepository.findById(id).orElse(null);
    }

    public List<Product> getProduct() {
        return productRepository.findAll();
    }

    public void deleteProduct(UUID id) {
        productRepository.deleteById(id);
    }

    public Optional<Product> updateProduct(UUID id, Product newProduct) {
        Optional<Product> oldProductOptional = productRepository.findById(id);
        if (oldProductOptional.isPresent()) {

            Product oldProduct = oldProductOptional.get();
            oldProduct.setName(newProduct.getName());
            oldProduct.setDescription(newProduct.getDescription());
            oldProduct.setQuantity(newProduct.getQuantity());
            oldProduct.setStockAlert(newProduct.getStockAlert());
            oldProduct.setExpiryDate(newProduct.getExpiryDate());
            oldProduct.setImageUrl(newProduct.getImageUrl());
            oldProduct.setVariety(newProduct.getVariety());
            oldProduct.setBarCode(newProduct.getBarCode());
            oldProduct.setUnit(newProduct.getUnit());
            oldProduct.setCategory(newProduct.getCategory());

            return Optional.of(productRepository.save(oldProduct));
        } else {
            return Optional.empty();
        }
    }

    public List<Product> getBellowStockProducts() {
        return productRepository.getBellowStockProducts();
    }

    public List<Product> getExpiredProduct() {
        return productRepository.getExpiredProduct(LocalDate.now());
    }

    public List<Product> searchByKeyword(String keyword) {
        return productRepository.searchByKeyword(keyword);
    }

    public void store(List<StockMovementDto> products) {

        // Solution optimale

//        Set<UUID> productsId =  products.stream()
//                .map(MovementDto::getProductId)
//                .collect(Collectors.toSet());
//
//        List<Product> productsToUpdate = productRepository.findAllById(productsId);
//        // TODO: Update products quantities
//        productRepository.saveAll(productsToUpdate);


       /* products.forEach(stockMovementDto -> {
            Product product = getProduct(stockMovementDto.getProductId()); // 1
            if (product != null) {
                product.setQuantity(product.getQuantity() + stockMovementDto.getQuantity());
                productRepository.save(product); // 2
            }
        });*/
    }

    public void removeProductsFromStock(StockMovementDto stockMovementDto) {

        // 1. Je fabrique l'entité mouvement de stock
        StockMovement stockMovement = StockMovement.builder()
                .observation(stockMovementDto.getObservation())
                .date(LocalDate.now())
                .designation(stockMovementDto.getDesignation())
                .type(EnumMovementType.EXIT)
                .build();
        StockMovement savedStockMovement = stockMovementRepository.save(stockMovement);

        // 2. Je fabrique les ligne de mon mouvement de stock, puis je leur ajoute le mouvement de
        // stock précédent
        stockMovementDto.getLines().forEach(stockMovementLineDto -> {
            Product product = getProduct(stockMovementLineDto.getProductId());
            if (product != null) {
                StockMovementLine stockMovementLine = StockMovementLine.builder()
                        .product(product)
                        .stockMovement(savedStockMovement)
                        .price(stockMovementLineDto.getPrice())
                        .quantity(stockMovementLineDto.getQuantity())
                        .date(LocalDateTime.now())
                        .build();
                stockMovementLineRepository.save(stockMovementLine);

                product.setQuantity(product.getQuantity() - stockMovementLineDto.getQuantity());
                productRepository.save(product);
            }
        });
    }

    public void addProductsToStock(StockMovementDto stockMovementDto) {

        // 1. Je fabrique l'entité mouvement de stock
        StockMovement stockMovement = StockMovement.builder()
                .observation(stockMovementDto.getObservation())
                .date(LocalDate.now())
                .designation(stockMovementDto.getDesignation())
                .type(EnumMovementType.ENTRANCE)
                .build();
        StockMovement savedStockMovement = stockMovementRepository.save(stockMovement);

        // 2. Je fabrique les ligne de mon mouvement de stock, puis je leur ajoute le mouvement de
        // stock précédent
        stockMovementDto.getLines().forEach(stockMovementLineDto -> {
            Product product = getProduct(stockMovementLineDto.getProductId());
            if (product != null) {
                StockMovementLine stockMovementLine = StockMovementLine.builder()
                        .product(product)
                        .stockMovement(savedStockMovement)
                        .price(stockMovementLineDto.getPrice())
                        .quantity(stockMovementLineDto.getQuantity())
                        .date(LocalDateTime.now())
                        .build();
                stockMovementLineRepository.save(stockMovementLine);

                product.setQuantity(product.getQuantity() + stockMovementLineDto.getQuantity());
                productRepository.save(product);
            }
        });
    }
}
