package com.example.inventoryserver.model.dto;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage {

    private String message;
    private Integer code;
    private Object data;

}
