package com.example.inventoryserver.model.dto;

import com.example.inventoryserver.model.enums.EnumProductUnit;
import lombok.*;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ProductDto {

    private UUID categoryId;

    private UUID id;
    private String name;
    private String description;
    private Integer quantity;
    private Integer stockAlert;
    private Double price;
    private LocalDate expiryDate;
    private String imageUrl;
    private String variety;
    private String barCode;
    private EnumProductUnit unit;
}
