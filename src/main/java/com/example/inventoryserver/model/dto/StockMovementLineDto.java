package com.example.inventoryserver.model.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StockMovementLineDto {

    private UUID id;
    private UUID productId;
    private Integer quantity;
    private Double price;
}
