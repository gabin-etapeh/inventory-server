package com.example.inventoryserver.model.dto;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StockMovementDto {

    private UUID id;
    private String designation;
    private String observation;
    private List<StockMovementLineDto> lines;

}
