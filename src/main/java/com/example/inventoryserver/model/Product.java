package com.example.inventoryserver.model;

import com.example.inventoryserver.model.enums.EnumProductUnit;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private String name;
    private String description;
    private Integer quantity;
    private Integer stockAlert;
    private Double price;
    private LocalDate expiryDate;
    private String imageUrl;
    private String variety;
    private String barCode;


    @Enumerated (EnumType.STRING)
    private EnumProductUnit unit;

    @ManyToOne (targetEntity = Category.class,
            cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    } )
    private Category category;


}
