package com.example.inventoryserver.model;

import com.example.inventoryserver.model.enums.EnumMovementType;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class StockMovement {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private String designation;
    private String observation;
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    private EnumMovementType type;

    @ManyToOne (targetEntity = Supplier.class)
    private Supplier supplier;

    @ManyToOne (targetEntity = InventoryUser.class)
    private InventoryUser inventoryUser;

    @OneToMany (mappedBy = "stockMovement")
    private List<StockMovementLine> movementLines;
}
