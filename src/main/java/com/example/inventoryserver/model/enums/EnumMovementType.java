package com.example.inventoryserver.model.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum EnumMovementType {
    ENTRANCE,
    EXIT
}
