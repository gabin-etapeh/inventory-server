package com.example.inventoryserver.model;

import jakarta.persistence.*;
import lombok.*;


import java.lang.annotation.Target;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@Entity
public class StockMovementLine {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private Integer quantity;
    private Double price;
    private LocalDateTime date;

    @ManyToOne (targetEntity = StockMovement.class)
    private StockMovement stockMovement;

    @OneToOne (targetEntity = Product.class)
    private Product product;
}
