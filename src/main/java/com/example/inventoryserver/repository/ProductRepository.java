package com.example.inventoryserver.repository;

import com.example.inventoryserver.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {

    @Query(value = "SELECT p FROM Product p WHERE p.quantity < p.stockAlert")
    List<Product> getBellowStockProducts();
   @Query(value = "SELECT p FROM Product p WHERE p.category.id = :category")
    List<Product> getProductOnCategory(@Param("category") UUID category);

    @Query(value = "SELECT p FROM Product p WHERE p.expiryDate < :currentDate")
    List<Product> getExpiredProduct(@Param("currentDate") LocalDate currentDate);

    @Query(value = "SELECT p FROM Product p WHERE p.name LIKE %:keyword% OR " +
            "p.description LIKE %:keyword% OR p.variety LIKE %:keyword%")
    List<Product> searchByKeyword(@Param("keyword") String keyword);

    List<Product> findByVariety(String variety);

    List<Product> findByNameAndVariety(String name, String variety);

}
