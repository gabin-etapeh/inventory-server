package com.example.inventoryserver.controller;

import com.example.inventoryserver.model.Product;
import com.example.inventoryserver.model.dto.StockMovementDto;
import com.example.inventoryserver.model.dto.ProductDto;
import com.example.inventoryserver.model.dto.ResponseMessage;
import com.example.inventoryserver.repository.ProductRepository;
import com.example.inventoryserver.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ProductRepository productRepository;

    @GetMapping("/product/{id}")
    public ResponseEntity<ResponseMessage> getProduct(@PathVariable UUID id) {
        ResponseMessage responseMessage = ResponseMessage.builder()
                .message("Product retrieved successfully")
                .code(200)
                .data(productService.getProduct(id))
                .build();
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    @PostMapping("/product")
    public ResponseEntity<Product> addProduct(@RequestBody ProductDto productDto) {
        return new ResponseEntity<>(productService.addProduct(productDto), HttpStatus.CREATED);
    }

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts() {
        return new ResponseEntity<>(productService.getProduct(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/product/{id}")
    public ResponseEntity<Integer> deleteProduct(@PathVariable UUID id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<ResponseMessage> updateProduct(@PathVariable UUID id,
                                                         @RequestBody Product newProduct) {
        return productService.updateProduct(id, newProduct)
                .map(product -> new ResponseEntity<>(ResponseMessage.builder()
                        .message("Product with id " + id + " updated successfully")
                        .code(200)
                        .data(product)
                        .build(), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(ResponseMessage.builder()
                        .message("No product to update")
                        .code(200)
                        .build(), HttpStatus.NOT_FOUND));
    }

    @GetMapping("/products/bellowStock")
    public ResponseEntity<ResponseMessage> getBellowStockProducts() {
        return new ResponseEntity<>(ResponseMessage.builder()
                .message("Product list")
                .code(200)
                .data(productService.getBellowStockProducts())
                .build(), HttpStatus.OK);
    }

    @GetMapping("/products/expired")
    public ResponseEntity<List<Product>> getExpiredProducts() {
        return new ResponseEntity<>(productService.getExpiredProduct(), HttpStatus.OK);
    }

    @GetMapping("/products/search")
    public ResponseEntity<List<Product>> searchProductByKeyword(@RequestParam String keyword) {
        return new ResponseEntity<>(productService.searchByKeyword(keyword), HttpStatus.OK);
    }

    @PostMapping("/products/store")
    public ResponseEntity<ResponseMessage> storeProducts(@RequestBody List<StockMovementDto> products) {
        productService.store(products);
        return new ResponseEntity<>(ResponseMessage.builder()
                .message("Products stored successfully")
                .code(200)
                .build(), HttpStatus.OK);
    }

    @PostMapping("/products/reduce-stock")
    public ResponseEntity<ResponseMessage> removeProductsFromStock(@RequestBody StockMovementDto stockMovementDto) {
        productService.removeProductsFromStock(stockMovementDto);
        return new ResponseEntity<>(ResponseMessage.builder()
                .message("Products removed successfully")
                .code(200)
                .build(), HttpStatus.OK);
    }

    @PostMapping("/products/add-stock")
    public ResponseEntity<ResponseMessage> addProductsToStock(@RequestBody StockMovementDto stockMovementDto) {
        productService.addProductsToStock(stockMovementDto);
        return new ResponseEntity<>(ResponseMessage.builder()
                .message("Products added successfully")
                .code(200)
                .build(), HttpStatus.OK);
    }
}
