package com.example.inventoryserver.controller;

import com.example.inventoryserver.service.CategoryService;
import com.example.inventoryserver.model.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/category/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable UUID id) {
        return new ResponseEntity<>(categoryService.getCategory(id), HttpStatus.OK);
    }
    @PostMapping("/category")
    public ResponseEntity<Category> addCategory(@RequestBody Category category) {
        return new ResponseEntity<>(categoryService.addCategory(category), HttpStatus.CREATED);
    }

    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getCategories() {
        return new ResponseEntity<>(categoryService.getCategories(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/category/{id}")
    public ResponseEntity<Integer> deleteCategory(@PathVariable UUID id) {
        categoryService.deleteCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable UUID id,
                                         @RequestBody Category nouvelleCategory) {
        Optional<Category> updatedCategory = categoryService.updateCategory(id, nouvelleCategory);
        if (updatedCategory.isPresent()) {
            return new ResponseEntity<>(updatedCategory.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucune catégorie à modifier", HttpStatus.NOT_FOUND);
        }
    }
}
