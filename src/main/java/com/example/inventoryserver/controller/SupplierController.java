package com.example.inventoryserver.controller;

import com.example.inventoryserver.model.Supplier;
import com.example.inventoryserver.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SupplierController {
    private final SupplierService supplierService;

    @GetMapping("/supplier/{id}")
    public ResponseEntity<Supplier> getSupplier(@PathVariable UUID id) {
        return new ResponseEntity<>(supplierService.getSupplier(id), HttpStatus.OK);
    }
    @PostMapping("/supplier")
    public ResponseEntity<Supplier> addSupplier(@RequestBody Supplier supplier) {
        return new ResponseEntity<>(supplierService.addSupplier(supplier), HttpStatus.CREATED);
    }

    @GetMapping("/suppliers")
    public ResponseEntity<List<Supplier>> getSuppliers() {
        return new ResponseEntity<>(supplierService.getSuppliers(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/supplier/{id}")
    public ResponseEntity<Integer> deleteCategory(@PathVariable UUID id) {
        supplierService.deleteSupplier(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/supplier/{id}")
    public ResponseEntity<?> updateSupplier(@PathVariable UUID id,
                                            @RequestBody Supplier nouveauFournisseur) {
        Optional<Supplier> updatedSupplier = supplierService.updateSupplier(id, nouveauFournisseur);
        if (updatedSupplier.isPresent()) {
            return new ResponseEntity<>(updatedSupplier.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucun fournisseur à modifier", HttpStatus.NOT_FOUND);
        }
    }

}
